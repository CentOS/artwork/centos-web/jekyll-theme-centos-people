all:

.PHONY: install-sources
install-sources:
	# -------------------------------------------------------------------------------
	# Includes
	# -------------------------------------------------------------------------------
	install -D -t examples/jekyll/_includes/people src/HTML/jekyll/_includes/*
	# -------------------------------------------------------------------------------
	# Layouts
	# -------------------------------------------------------------------------------
	install -D -t examples/jekyll/_layouts/people src/HTML/jekyll/_layouts/*
	# -------------------------------------------------------------------------------
	# Data
	# -------------------------------------------------------------------------------
	install -D -t examples/jekyll/_data/people/ src/YAML/navbar.yml
	install -D -t examples/jekyll/_data/people/username/ src/YAML/projects.yml
	# -------------------------------------------------------------------------------
	# Images
	# -------------------------------------------------------------------------------
	install -D -t examples/jekyll/assets/img/people/username/ src/IMG/*

html-validate: clean install-sources
	install -d public
	podman run --rm \
		-v $$PWD/public:/public \
		-v $$PWD/examples/jekyll:/site \
		--name jekyll-theme-centos \
		registry.gitlab.com/centos/artwork/centos-web/jekyll-theme-centos:latest \
		bundle exec jekyll build --config /site/_config.yml -s /site -d /public
	npx prettier --ignore-path .prettierignore -w public/
	npx html-validate public/

.PHONY: jekyll-server
jekyll-server: install-sources
	# -------------------------------------------------------------------------------
	# Public
	# -------------------------------------------------------------------------------
	install -d public
	# -------------------------------------------------------------------------------
	# Run server using jekyll-theme-centos:latest container image
	# -------------------------------------------------------------------------------
	podman run --rm \
		-v $$PWD/public:/public \
		-v $$PWD/examples/jekyll:/site \
		-p 0.0.0.0:4000:4000 \
		--name jekyll-theme-centos \
		registry.gitlab.com/centos/artwork/centos-web/jekyll-theme-centos:latest \
		bundle exec jekyll serve -H 0.0.0.0 -p 4000 --config /site/_config.yml -s /site -d /public

.PHONY: clean
clean:
	$(RM) -r examples/jekyll/_includes
	$(RM) -r examples/jekyll/_layouts
	$(RM) -r examples/jekyll/_data
	$(RM) -r examples/jekyll/_sass
	$(RM) -r examples/jekyll/assets
	$(RM) -r public
