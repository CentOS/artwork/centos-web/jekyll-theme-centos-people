---
title: People of CentOS | YourName
layout: people/default

username: username
fullname: full name
avatar: avatar.svg

with_highlight: stackoverflow-light
---

Pellentesque dapibus suscipit ligula. Donec posuere augue in quam. Etiam vel
tortor sodales tellus ultricies commodo. Suspendisse potenti. Aenean in sem
ac leo mollis blandit. Donec neque quam, dignissim in, mollis nec, sagittis
eu, wisi. Phasellus lacus. Etiam laoreet quam sed arcu. Phasellus at dui in
ligula mollis ultricies. Integer placerat tristique nisl. Praesent augue.
Fusce commodo.

# Projects

On my free time, I contribute to the following opensource projects:

{% assign projects = site.data.people[page.username].projects %}
{% include people/projects.html projects=projects %}

If you value my work and want to support me, you can [buy me a cup of coffee](#).

# GPG Keys

To verify my signed e-mails, use the following GPG public key:

```
...
```
