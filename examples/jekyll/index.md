---
title: "jekyll-theme-centos-people"
title_lead: "HTML templates, YAML files and images related to jekyll-theme-centos-component."

with_manifestation: "jekyll-theme-centos-people"
with_title: false
with_preamble: false
with_announcements: {}
with_breadcrumbs: false
with_toc: false
with_artwork: false
with_content: false
with_shortcuts: false
with_sponsors: false
with_social: true
with_finale: true
with_copyright: true
with_highlight: false
with_datatables: false
---
